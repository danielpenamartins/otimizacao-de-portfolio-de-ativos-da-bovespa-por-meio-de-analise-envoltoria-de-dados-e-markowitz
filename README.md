# OTIMIZAÇÃO DE PORTFÓLIO DE ATIVOS DA BOVESPA POR MEIO DE ANÁLISE ENVOLTÓRIA DE DADOS E MARKOWITZ

**Daniel Pena Martins** – [danielpenamartins@gmail.com](mailto:danielpenamartins@gmail.com)  
**Gisele Tessari Santos** – [gisele.santos@ibmec.edu.br](mailto:gisele.santos@ibmec.edu.br)

Artigo submetido ao [XXII Encontro Nacional de Modelagem Computacional](http://enmc.fadepe.org.br/).

***Resumo:*** *O mercado de ações torna-se cada vez mais reconhecido pelo brasileiro como alternativa de investimento. Por se tratar de um investimento de renda variável, diversas métricas de variabilidade e técnicas de otimização têm sido criadas ou adaptadas para fins de composição de portfólios de alta performance. O presente estudo teve como objetivo a aplicação de algumas dessas técnicas na composição de 3 carteiras otimizadas distintas com ativos da Bovespa. Foram utilizadas a Análise por Envoltória de dados para seleção de ativos eficientes e a otimização de portfólio de Markowitz, com função objetivo de minimizar o risco a partir de duas métricas diferentes: a variância da carteira e o valor em risco condicional (CVaR). A performance das carteiras foi analisada de acordo com o fechamento do pregões no último ano, apresentando retornos cerca de duas vezes melhores que os do índice Bovespa. A carteira construída a partir de CVaR foi a que apresentou melhor retorno no período, provando que mesmo com todas adversidades observadas no período a incorporação de métodos quantitativos refinados pôde melhorar o resultado em relação a métodos simples. A fim de contornar a incerteza do mercado propõe-se que o investidor atualize seus modelos com dados recentes constantemente.*

## Organização do Projeto

Os programas utilizados foram o RStudio, SIAD, Excel e Tableau.

Arquivo/Pasta | Descrição
--------|-----------
[Artigo_2019_Martins e Santos_ENMC_OTIMIZAÇÃO DE PORTFÓLIO DE ATIVOS DA BOVESPA.pdf](https://gitlab.com/danielpenamartins/otimizacao-de-portfolio-de-ativos-da-bovespa-por-meio-de-analise-envoltoria-de-dados-e-markowitz/blob/master/2019_Martins%20e%20Santos_ENMC_FINAL_OTIMIZA%C3%87%C3%83O%20DE%20PORTF%C3%93LIO%20DE%20ATIVOS%20DA%20BOVESPA.pdf) | Artigo em .pdf.
[**Artigos**](https://gitlab.com/danielpenamartins/otimizacao-de-portfolio-de-ativos-da-bovespa-por-meio-de-analise-envoltoria-de-dados-e-markowitz/tree/master/Artigos/) | Pasta com todos os artigos usados como referência no projeto.
[**Scripts**](https://gitlab.com/danielpenamartins/otimizacao-de-portfolio-de-ativos-da-bovespa-por-meio-de-analise-envoltoria-de-dados-e-markowitz/tree/master/Scripts/ "Pasta de Scripts") | Pasta com todos Scripts usados para extração e transformação de dados.
[**CSV**](https://gitlab.com/danielpenamartins/otimizacao-de-portfolio-de-ativos-da-bovespa-por-meio-de-analise-envoltoria-de-dados-e-markowitz/tree/master/CSV/) | Pasta com todos arquivos .csv gerados a partir de Scripts.
[**TXT**](https://gitlab.com/danielpenamartins/otimizacao-de-portfolio-de-ativos-da-bovespa-por-meio-de-analise-envoltoria-de-dados-e-markowitz/tree/master/TXT/) | Pasta com todos arquivos .txt, que são o formato usado para inputs e outputs de dados no programa SIAD.
[Poster_ENMC_ECTM_2019.pdf](https://gitlab.com/danielpenamartins/otimizacao-de-portfolio-de-ativos-da-bovespa-por-meio-de-analise-envoltoria-de-dados-e-markowitz/blob/master/Poster_ENMC_ECTM_2019.pdf) | PDF do pôster apresentado.
[Siadv3.zip](https://gitlab.com/danielpenamartins/otimizacao-de-portfolio-de-ativos-da-bovespa-por-meio-de-analise-envoltoria-de-dados-e-markowitz/blob/master/Siadv3.zip) | Arquivos do programa SIAD compilados.
[6.portfolios-5-1.xlsx](https://gitlab.com/danielpenamartins/otimizacao-de-portfolio-de-ativos-da-bovespa-por-meio-de-analise-envoltoria-de-dados-e-markowitz/blob/master/6.portfolios-5-1.xlsx) | Excel que compila a composição de cada carteira, bem como o uso do Solver para otimizações.
[8.Analysis.twb](https://gitlab.com/danielpenamartins/otimizacao-de-portfolio-de-ativos-da-bovespa-por-meio-de-analise-envoltoria-de-dados-e-markowitz/blob/master/8.Analysis.twb) | Arquivo no Tableau para construção dos gráficos utilizados no projeto e demais cálculos da análise de 12 meses da performance das carteiras.
[Apendice.md](https://gitlab.com/danielpenamartins/otimizacao-de-portfolio-de-ativos-da-bovespa-por-meio-de-analise-envoltoria-de-dados-e-markowitz/blob/master/apendice.md) | Tabela contendo Ativos da carteira MLCX analisados, dados de entradas (inputs) e saídas (outputs) e resultados da análise da eficiência por meio de DEA-BCC.
[README.md](https://gitlab.com/danielpenamartins/otimizacao-de-portfolio-de-ativos-da-bovespa-por-meio-de-analise-envoltoria-de-dados-e-markowitz/blob/master/README.md "Esse próprio arquivo README") | Esse próprio arquivo README
