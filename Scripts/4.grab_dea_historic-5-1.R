library(tidyverse) # library(dplyr); library(purrr)

#After DEA result:
dearesult <- readr::read_csv("CSV/3.dea.efficiency-5-1.csv")
dearesult <- filter(dearesult, Padrao == 1)
xts.df <- readr::read_csv("CSV/1.xts_list.csv")
xts.df <- filter(xts.df, date > "2013-05-17" & date < '2018-05-17')
xtsdea  <- xts.df %>% filter(name == dearesult$DMU[1])
for (i in 2:nrow(dearesult)){
   xtsdea2 <- xts.df %>% filter(name == dearesult$DMU[i])
   xtsdea <- union(xtsdea, xtsdea2)
}
rm(xtsdea2)
rm(xts.df)
#get daily returns of a stock
for (i in 2:nrow(xtsdea)){xtsdea$r[i] <- (xtsdea$x[i]-xtsdea$x[i-1])/xtsdea$x[i-1]}
for (i in 1:nrow(xtsdea)){
  if (xtsdea$date[i] == '2013-05-20'){xtsdea$r[i] <- NA}}
rm(i)
#get history for first efficient unit
histvalues <- filter(xtsdea, name == dearesult$DMU[1])
histvalues <- histvalues  %>% transmute(date, r)
histvalues <- histvalues %>% rename_all(funs(str_replace(., "r", sprintf("%s", dearesult$DMU[1]))))
#get history for other efficient units and merge into 1 df
for (i in 2:nrow(dearesult)){
   histvalues2 <- filter(xtsdea, name == dearesult$DMU[i])
   histvalues2 <- histvalues2  %>% transmute(date, r)
   histvalues2 <- histvalues2 %>% rename_all(funs(str_replace(., "r", sprintf("%s", dearesult$DMU[i]))))
   histvalues <- merge.data.frame( histvalues, histvalues2, by.x="date", by.y="date")
}
histvalues2 <- filter(xtsdea, name == dearesult$DMU[1])
histvalues2 <- histvalues2 %>% transmute(date, x)
histvalues2 <- histvalues2 %>% rename_all(funs(str_replace(., "x", sprintf("%sV", dearesult$DMU[1]))))
for (i in 2:nrow(dearesult)){
   histvalues3 <- filter(xtsdea, name == dearesult$DMU[i])
   histvalues3 <- histvalues3 %>% transmute(date, x)
   histvalues3 <- histvalues3 %>% rename_all(funs(str_replace(., "x", sprintf("%sV", dearesult$DMU[i]))))
   histvalues2 <- merge.data.frame( histvalues2, histvalues3, by.x="date", by.y="date")
}
histvalues <- merge.data.frame( histvalues, histvalues2, by.x="date", by.y="date")
rm(xtsdea)
rm(histvalues2)
rm(histvalues3)
rm(i)
rm(dearesult)
#Save results in csv
readr::write_csv(histvalues, "CSV/4.dea.historic-5-1.csv")
rm(histvalues)
