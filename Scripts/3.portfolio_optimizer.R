# install.packages("nloptr")

library(nloptr)
library(tidyverse) # library(dplyr); library(purrr)

#Check working directory to pick and write the CSVs
getwd()
setwd('/users/danie/Desktop/projects/otimizacao-de-portfolio-de-ativos-da-bovespa-por-meio-de-analise-envoltoria-de-dados-e-markowitz')

dea <- readr::read_csv("CSV/2.dea.csv")

#the portfolio will use only efficient tickers in composition
portfolio <- dea %>% filter(eff == 1) 
portfolio <-portfolio[names(portfolio) %in% c("name","r_std_1","r_avg_1")]  

#the weights of every ticker start the same (1/n)
neff = portfolio %>% count
neff = neff[[1]]
portfolio$w <- 1/neff
rm(neff)

#build the covariance matrix to assist with calculations
covar <- portfolio$r_std_1 %*% t(portfolio$r_std_1)

#build both the return and variance of firs portfolio (ly)
r_0 = portfolio %>% summarise(sum(r_avg_1*w))
r_0 = r_0[[1]]
s_0 = (portfolio$w %*% t(portfolio$w) * covar) %>% sum()

#wrap the weights and metrics into portfolio1
portfolio1 = list(portfolio,r_0,s_0)

#create optmization function that is to lower the variance maintaining r0
eval_f <- function( x ) {
  return( list( "objective" = c(sum(x%*%t(x)*covar))
                , "gradient" = c(2*sum(covar[1,]%*%x),
                                 2*sum(covar[2,]%*%x),
                                 2*sum(covar[3,]%*%x),
                                 2*sum(covar[4,]%*%x),
                                 2*sum(covar[5,]%*%x),
                                 2*sum(covar[6,]%*%x))
  ) ) }

# constraint functions
# inequalities
eval_g_ineq <- function( x ) {
  constr <- r_0 - sum(portfolio$r_avg_1*x) 
  grad <- c( -portfolio$r_avg_1 )
  return( list( "constraints"=constr, "jacobian"=grad 
  ) )}

# equalities
eval_g_eq <- function( x ) {
  constr <- sum(x) - 1
  grad <- c( 1,1,1,1,1,1 )
  return( list( "constraints"=constr, "jacobian"=grad 
  ) )}

# initial values
x0 <- c( 1, 0, 0, 0, 0, 0 )
# lower and upper bounds of control
lb <- c( 0, 0, 0, 0, 0, 0 )
ub <- c( 1, 1, 1, 1, 1, 1 )
local_opts <- list( "algorithm" = "NLOPT_LD_MMA",
                    "xtol_rel" = 1.0e-12 )

"is important to say that 3 algorithms where tested, NLOPT_GN_ISRES is genetical
NLOPT_LD_AUGLAG and NLOPT_LD_MMA are lagrangians, so they will need the gradients.
The most sucessful in the test was ISRES, although it did take a lot to process,
since it was needed to put a very high maxeval to reach a satisfatory answer."

opts <- list( "algorithm" = "NLOPT_GN_ISRES",
              "xtol_rel" = 1.0e-12,
              "maxeval" = 100000,
              "local_opts" = local_opts )
res <- nloptr( x0=x0,
               eval_f=eval_f,
               lb=lb,
               ub=ub,
               eval_g_ineq=eval_g_ineq,
               eval_g_eq=eval_g_eq,
               opts=opts)
print( res )

#save the other result
r_1 = portfolio %>% summarise(sum(r_avg_1*w))
r_1 = r_1[[1]]
s_1 = (portfolio$w %*% t(portfolio$w) * covar) %>% sum()


portfolio2 = list(portfolio,r_1,s_1)
